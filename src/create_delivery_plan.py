import csv
import time
import datetime
from unidecode import unidecode
import pandas as pd
import get_info_master

data_string_template = "{0},{1},{2},{3},,{4},{5},{6},{7},{7},{6},{5},{8},{9},{10},{11},{12}"
TODAY = int(time.mktime(datetime.datetime.today().timetuple()) * 1000)
TODAY_DATE = datetime.datetime.fromtimestamp(TODAY / 1000).strftime('%Y-%m-%d')
TOMORROW = int(time.mktime((datetime.datetime.today() +
                            datetime.timedelta(days=1)).timetuple()) * 1000)
TOMORROW_DATE = datetime.datetime.fromtimestamp(
    TOMORROW / 1000).strftime('%Y-%m-%d')


def get_product_uom_info(product_code):
    uom = ""
    uom_size = ""
    upc = ""
    uom_name = ""
    refill_uom_id = get_info_master.get_refill_uom_info(
        product_code, "refill_uom_id")
    product_uom = get_info_master.get_product_uom(product_code)
    for row in product_uom:
        if row['id'] == refill_uom_id:
            uom = row['uom']
            upc = row['upc']
            uom_size = row['uom_size']
            uom_name = row['uom_name']
    return uom, uom_size, upc, uom_name


def input_info_template(
        product_code,
        quantity=0,
        exp_date=1544115600000,
        supplier_delivery_id='0'):
    # get info from product code
    product_name = get_info_master.get_product_info(product_code, 'short_name')
    product_name = unidecode(product_name)
    supplier_name = get_info_master.get_supplier_mapping_info(
        product_code, 'supplier_name')
    supplier_code = get_info_master.get_supplier_info(
        supplier_name, 'supplier_code')
    supplier_name = unidecode(supplier_name)

    prod_uom, prod_uom_size, prod_upc, prod_uom_name = get_product_uom_info(
        product_code)
    uom_name_vn = get_info_master.get_refill_uom_info(
        product_code,
        'refill_uom_name_vn'
    )

    data_string = data_string_template.format(
        product_code,
        product_name,
        supplier_code,
        supplier_name,
        quantity,
        int(quantity / prod_uom_size),
        prod_uom,
        int(prod_uom_size),
        exp_date,
        TOMORROW,
        supplier_delivery_id,
        unidecode(uom_name_vn),
        prod_upc)

    return data_string


def create_inventory_receive_data(data_json):
    data = []
    product_code = 0000000
    quantity = 0
    exp_date = 1544115600000
    supplier_delivery_id = 'SZZZZZZZQ'

    for i in data_json:
        if 'product_code' in i:
            product_code = i['product_code']
        if 'quantity' in i:
            quantity = i['quantity']
        if 'exp_date' in i:
            exp_date = i['exp_date']
        if 'supplier_delivery_id' in i:
            supplier_delivery_id = i['supplier_delivery_id']

        data_row = input_info_template(
            product_code=product_code,
            quantity=quantity,
            exp_date=exp_date,
            supplier_delivery_id=supplier_delivery_id)

        data.append(data_row)

    return data


def create_inventory_receive(
        file_name=None,
        list_product_code=None):
    with open('temp_receive.csv', 'w') as csvfile:
        spamwriter = csv.writer(
            csvfile, delimiter=',',
            quotechar='"',
            quoting=csv.QUOTE_NONNUMERIC)

        list_header = "product_id,product_short_name_vn,supplier_id,\
                    supplier_name,supplier_product_id,base_unit_quantity,\
                    refill_unit_quantity,refill_uom_code,refill_uom_size,\
                    store_order_uom_size,store_order_uom_code,\
                    order_quantity_store_order_unit,expiry_date,\
                    delivery_date,supplier_delivery_id,store_order_uom_name,\
                    store_order_uom_upc"
        list_header = map(str.strip, list_header.split(sep=','))
        spamwriter.writerow(list_header)

        if list_product_code is not None:
            data = create_inventory_receive_data(list_product_code)
            for i in data:
                row = map(str.strip, str(i).split(sep=','))
                spamwriter.writerow(row)

        print(spamwriter)


def format_date(str_date):
    if str_date != "":
        date = str.strip(str_date)
        date = datetime.datetime.strptime(date, "%d/%m/%Y")
        date_timestamp = int(time.mktime(date.timetuple())*1000)
        return date_timestamp
    else:
        return ""


if __name__ == "__main":
    data_json = pd.DataFrame(pd.read_excel("delivery_plan.xlsx"))
    data_json.exp_date = data_json.exp_date.apply(lambda x: format_date(x))
    list_product_info = data_json.to_dict(orient='records')
    print(list_product_info)
    create_inventory_receive(list_product_code=list_product_info)
