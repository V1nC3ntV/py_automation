import api_request
import config
import ast

url_backend = config.DEFAULT_ENV['hq-backend']
url_purchase_order_supplier = url_backend + "/api/v2/portal/logistics/store_order/purchase_order_suppliers"
url_purchase_order_detail = url_backend + "/api/v2/portal/logistics/store_order/purchase_order_details"
url_purchase_order_export = url_backend + "/api/v2/portal/logistics/store_order/purchase_order_suppliers_export"

email = "hoangviet@msv-tech.vn"
password = "12345678"


def get_supplier_list(delivery_data):
    supplier_query = "{{'delivery_date': '{0}'}}".format(delivery_data)
    supplier_list = api_request.get_request_logistic(
        str.strip(url_purchase_order_supplier),
        email=email,
        password=password,
        query_string=supplier_query)

    return supplier_list


def get_purchase_order_detail(delivery_data, supplier_id):
    purchase_order_detail_query = "{{'delivery_date': '{0}','supplier_id': '{1}'}}".format(delivery_data, supplier_id)

    purchase_order_detail = api_request.get_request_logistic(
        str.strip(url_purchase_order_detail),
        email=email,
        password=password,
        query_string=purchase_order_detail_query)

    # print(purchase_order_detail)
    return purchase_order_detail


def get_purchase_order_detail_export(delivery_date):
    delivery_date_query = "{{'delivery_date': '{0}', 'file_type': 'csv'}}".format(delivery_date)
    purchase_order_detail_export = api_request.get_request_logistic(
        str.strip(url_purchase_order_export),
        email=email,
        password=password,
        query_string=delivery_date_query)

    # print(purchase_order_detail_export)
    return purchase_order_detail_export
