import csv
import math
import time
import datetime
import json
import pandas as pd
from collections import OrderedDict
from unidecode import unidecode
import get_info_master
import get_cdc_info

data_string_template = "{0},{1},{2},{3},,{4},{5},{6},{7},{7},{8},{7},{9}"
TODAY                = int(time.mktime(datetime.datetime.today().timetuple()) * 1000)
TODAY_DATE           = datetime.datetime.fromtimestamp(TODAY / 1000).strftime('%Y-%m-%d')
TOMORROW             = int(time.mktime((datetime.datetime.today() + datetime.timedelta(days = 1)).timetuple()) * 1000)
TOMORROW_DATE        = datetime.datetime.fromtimestamp(TOMORROW / 1000).strftime('%Y-%m-%d')


def input_info_template(
        product_code,
        cdc_code='101',
        quantity=0,
        exp_date=1544115600000,
        supplier_delivery_id='0',
        inventory_type='0'):
    # get info from product code
    product_name = get_info_master.get_product_info(product_code, 'short_name')
    product_name = unidecode(product_name)

    supplier_name = get_info_master.get_supplier_mapping_info(product_code, 'supplier_name')
    supplier_code = get_info_master.get_supplier_info(supplier_name, 'supplier_code')
    supplier_name = unidecode(supplier_name)

    data_string = data_string_template.format(
        product_code,
        product_name,
        supplier_code,
        supplier_name,
        exp_date,
        quantity,
        cdc_code,
        TODAY,
        supplier_delivery_id,
        inventory_type)

    return data_string


def create_inventory_receive_data(data_json):
    data = []
    product_code = 0000000
    cdc_code = '101'
    quantity = 0
    exp_date = 1544115600000
    supplier_delivery_id = 'SZZZZZZZQ'
    inventory_type = 0

    for i in data_json:
        if 'product_code' in i:
            product_code = i['product_code']
        if 'quantity' in i:
            quantity = i['quantity']
        if 'cdc_code' in i:
            cdc_code = i['cdc_code']
        if 'exp_date' in i:
            exp_date = i['exp_date']
        if 'supplier_delivery_id' in i:
            supplier_delivery_id = i['supplier_delivery_id']
        if 'inventory_type' in i:
            inventory_type = i['inventory_type']

        data_row = input_info_template(
            product_code=product_code,
            cdc_code=cdc_code,
            quantity=quantity,
            exp_date=exp_date,
            supplier_delivery_id=supplier_delivery_id,
            inventory_type=inventory_type)

        data.append(data_row)
    return data


def create_inventory_receive(
        file_name=None,
        list_product_code=None):
    with open('temp_receive.csv', 'w') as csvfile:
        spamwriter = csv.writer(
            csvfile, delimiter=',',
            quotechar='"',
            quoting=csv.QUOTE_NONNUMERIC)

        list_header = "product_id,product_short_name_vn,supplier_id,\
        supplier_name,supplier_product_id,expiry_date,quantity,cdc_id,\
        start_receiving_time,end_receiving_time,supplier_delivery_id,\
        submit_timestamp,inventory_type"
        list_header = map(str.strip, list_header.split(sep=','))
        spamwriter.writerow(list_header)

        if list_product_code is not None:
            data = create_inventory_receive_data(list_product_code)
            for i in data:
                row = map(str.strip, str(i).split(sep=','))
                spamwriter.writerow(row)

        print(spamwriter)


def xstr(s):
    if s != 'null':
        if s is None:
            return 'null'
        if math.isnan(s):
            return 'null'
    else:
        return s
    return int(s)


def format_date(str_date):
    if str_date != 'nan':
        date = datetime.datetime.strptime(str_date, '%Y-%m-%d')
        date_timestamp = int(time.mktime(date.timetuple()) * 1000)
        return date_timestamp
    else:
        return ''


def create_delivery_plan(refill_date):
    suppliers = get_cdc_info.get_cdc_suppliers(refill_date)
    delivery_plans = list()
    for row in suppliers:
        delivery_plans = delivery_plans + get_cdc_info.get_cdc_delivery_plan(
            row['supplier_id'],
            refill_date)

    # for row in delivery_plans:
    #     if row['supplier_feed_back']:
    #         for i in row['supplier_feed_back']:
    #             if i['status'] == 2:

    delivery_plans_pd = pd.DataFrame(pd.read_json(
        json.dumps(delivery_plans)))
    # print(delivery_plans_pd)
    # excel = pd.ExcelWriter('temp.xlsx')
    # delivery_plans_pd.to_excel(excel)
    # excel.save()

    info = {
        "product_code": "",
        "quantity": "",
        "supplier_delivery_id": "",
        "exp_date": ""
    }
    receiving = list()
    for index, row in delivery_plans_pd.iterrows():
        receiving_info = info.copy()
        receiving_info['product_code'] = row['product_code']
        supplier_feedbacks = row['supplier_feed_back']
        if supplier_feedbacks:
            for i in supplier_feedbacks:
                if i['status'] == 2:
                    receiving_info['quantity'] = i['quantity']
                    receiving_info['supplier_delivery_id'] = i['supplier_delivery_id']
                    receiving_info['exp_date'] = i['expiry_date']
                    receiving.append(receiving_info)
                    # print(receiving)

    # receiving = list(OrderedDict.fromkeys(receiving))
    receiving_pd = pd.DataFrame(receiving)
    receiving_pd = receiving_pd.drop_duplicates('product_code')
    receiving_pd['exp_date'] = receiving_pd['exp_date'].apply(str)
    receiving_final = receiving_pd.to_dict(orient='records')
    print(receiving_final)
    return receiving_final


if __name__ == "__main__":
    # data_json = pd.DataFrame(pd.read_excel("delivery_plan.xlsx"))
    # data_json.exp_date = data_json.exp_date.apply(str)
    # data_json.exp_date = data_json.exp_date.apply(lambda x: format_date(x))
    # list_product_info = data_json.to_dict(orient='records')
    # print(list_product_info)

    list_product_info = create_delivery_plan('2018-04-02')
    # print(list_product_info)
    create_inventory_receive(list_product_code=list_product_info)
    # create_inventory_receive(list_product_code=[
    #     {"product_code": 41003263, "quantity": 12},
    #     {"product_code": 55003977, "quantity": 30, }
    # ])
