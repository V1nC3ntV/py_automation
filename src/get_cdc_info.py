import api_request
import config

url_backend = config.UAT_ENV['hq-backend']
url_product_uom = url_backend + "/api/v2/hq/product_uoms/"
url_cdc_supplier = url_backend + "/api/v2/portal/cdc/refill/suppliers"
url_cdc_delivery = url_backend + "/api/v2/portal/cdc/refill/details"

email = "cdc101@ssv.com.vn"
password = "cdc101@ssv2017"


def get_cdc_suppliers(refill_date):
    query_string = "'refill_date': '{}'".format(refill_date)
    suppliers_list = api_request.get_request_cdc(
        url_cdc_supplier,
        email=email,
        password=password,
        query_string=query_string)
    return suppliers_list


def get_cdc_delivery_plan(supplier_id, refill_date):
    query_string = "'supplier_id': {},'refill_date': '{}'".format(
        supplier_id,
        refill_date)
    response = api_request.get_request_cdc(
        url_cdc_delivery,
        email=email,
        password=password,
        query_string=query_string)

    if response['refill_detail_dto_list']:
        refill_detail = response['refill_detail_dto_list']
    else:
        refill_detail = None
    return refill_detail
