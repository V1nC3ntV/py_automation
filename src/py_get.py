import api_request
import config

url = "http://localhost:8888" + "/api/v2/test/all_daily_inventory_consolidation"

str_hq_backend = "http://localhost:8888"
str_url = str_hq_backend + "/api/system/authentication"
str_url_logistic = str_hq_backend + "/api/v2/portal/logistics/authentication"
str_hq_authorization = str_hq_backend + "/api/v2/hq/authentication"
str_hq_categories = str_hq_backend + "/api/v2/hq/categories?&q=&type=1&\{\}"
str_cdc_inventory_consolidate = str_hq_backend + \
    "/api/v2/test/all_daily_inventory_consolidation"
str_api_productlist = str_hq_backend + "/api/v2/hq/products/"
str_api_product_uom = str_hq_backend + "api/v2/hq/product_uoms"

query_string = "'q': '97000001'"
contain = api_request.get_request(
    str_api_productlist, "admin@ssv.com", "123456", query_string)

# get content from response
# contain = response.json()
content = contain['content']
# print(type(content))

product_id = ""
for i in content:
    product_id = str(i['id'])
    # print(i['id'])

url_product = str_api_productlist + product_id

response_product_detail = api_request.get_request(
    url_product, "admin@ssv.com", "123456")

# get content from response
json_contain = response_product_detail
print(json_contain)
