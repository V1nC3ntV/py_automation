import pandas as pd

import glob

# Read csv file and add to data Frame
data = pd.DataFrame()
df = pd.read_csv("store_product_mapping_view_v2.csv")
data = data.append(df)

# Remove duplicated record
data = data.drop_duplicates(['product_id'], keep='last')

count = data['cdc_id'] == 2.0

print(count.value_counts())
