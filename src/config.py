# pylint: disable=C0111
DEV_ENV = {
    'hq-backend': "http://dev-hq-backend.seven-system.com",
    'hq-email': 'hoangviet@msv-tech.vn',
    'hq-pass': '12345678',
    'logistic-email': 'logistics@ssv.com',
    'logistic-pass': '123456',
    'supplier-email': 'hoangviet@siliconstraits.com',
    'supplier-pass': '12345678',
    'cdc1-email': 'cdc101@ssv.com.vn',
    'cdc1-pass': 'cdc101@ssv2017',
    'cdc2-email': 'cdc102@ssv.com.vn',
    'cdc2-pass': 'cdc102@ssv2017'
}

STAGING_ENV = {
    'hq-backend': "https://staging-backend.sevensystem.vn",
    'hq-email': 'hoangviet@msv-tech.vn',
    'hq-pass': '12345678',
    'logistic-email': 'logistics@ssv.com',
    'logistic-pass': '123456',
    'supplier-email': 'hoangviet@siliconstraits.com',
    'supplier-pass': '12345678',
    'cdc1-email': 'cdc101@ssv.com.vn',
    'cdc1-pass': 'cdc101@ssv2017',
    'cdc2-email': 'cdc102@ssv.com.vn',
    'cdc2-pass': 'cdc102@ssv2017'
}

UAT_ENV = {
    'hq-backend': "http://uat-hq-backend.seven-system.com",
    'hq-email': 'hoangviet@msv-tech.vn',
    'hq-pass': '12345678',
    'logistic-email': 'logistics@ssv.com',
    'logistic-pass': '123456',
    'supplier-email': 'hoangviet@siliconstraits.com',
    'supplier-pass': '12345678',
    'cdc1-email': 'cdc101@ssv.com.vn',
    'cdc1-pass': 'cdc101@ssv2017',
    'cdc2-email': 'cdc102@ssv.com.vn',
    'cdc2-pass': 'cdc102@ssv2017'
}

QA_ENV = {
    'hq-backend': "http://qa-hq-backend.seven-system.com",
    'hq-email': 'hoangviet@msv-tech.vn',
    'hq-pass': '12345678',
    'logistic-email': 'logistics@ssv.com',
    'logistic-pass': '123456',
    'supplier-email': 'hoangviet@siliconstraits.com',
    'supplier-pass': '12345678',
    'cdc1-email': 'cdc101@ssv.com.vn',
    'cdc1-pass': 'cdc101@ssv2017',
    'cdc2-email': 'cdc102@ssv.com.vn',
    'cdc2-pass': 'cdc102@ssv2017'
}

DEFAULT_ENV = DEV_ENV