import requests
import re
import hashlib
from zipfile import ZipFile
from datetime import timedelta, date
import time
import e

url = "http://uat-hq-backend.seven-system.com/api/v2/portal/cdc/export/store_receiving"

headers = {
    'origin': "https://cdc.sevensystem.vn",
    'accept-encoding': "gzip, deflate, br",
    'authorization': "Bearer ef6e760b-78c1-4cc0-b160-3778a696f292"}


def daterange(start_date, end_date):
    for n in range(int((end_date - start_date).days)):
        yield start_date + timedelta(n)

# Start run save files
start_date = date(2017, 6, 1)
end_date = date(2017, 8, 29)
for single_date in daterange(start_date, end_date):
    receiving_date = int(time.mktime(single_date.timetuple())) * 1000

    querystring = '''{{"single_page": "true", "receive_time":"{}"}}'''.format(
        receiving_date)
    querystring = e.literal_eval(querystring)
    print(querystring)

    response = None
    response = requests.get(url, headers=headers, params=querystring)

    print(response.url)
    # print(response.content)
    print(response.headers["Content-Disposition"])
    file_name = re.search('cdc_store_order_allocation(.+?)_102.zip',
                          str(response.headers["Content-Disposition"])).group(1)
    file_name = "cdc_store_order_allocation{}_102".format(file_name)
    print(file_name)
    with open('temp/' + file_name + '.zip', 'wb') as handle:
        handle.write(response.content)

    handle.close()

    m = hashlib.sha384(
        (file_name + 'rQndMPjAjSrLTeyD9oDgI6k5vTy0+Zo2rCimAm8J7CA=').encode('utf-8')).hexdigest()

    print(m)

    zipFile = ZipFile('temp/' + file_name + '.zip')
    extractFile = zipFile.extractall(pwd=str.encode(m), path='temp/')
