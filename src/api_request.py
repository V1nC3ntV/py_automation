import ast
import requests
import config


def get_request(url, email, password, query_string=""):
    json_string = "'email': \"{0}\", 'password': \"{1}\"".format(email, password)
    json = ast.literal_eval("{" + json_string + "}")

    url_authorization = config.DEFAULT_ENV['hq-backend'] + '/api/v2/hq/authentication'.strip()
    access_token = requests.post(url_authorization, None, json=json)
    access_token = access_token.json()
    access_token = access_token['access_token']

    str_header = "'content-type': \"application/json\",'authorization':\
                    \"bearer {0}\",'cache-control': \"no-cache\"".format(access_token)
    str_header = "{" + str_header + ", 'Accept':'application/json'}"

    # input query to param
    querystring = ast.literal_eval("{" + query_string + "}")
    payload = "{\n    \"email\": \"hoangviet@msv-tech.vn\",\n    \"password\": \"12345678\"\n}"
    headers = ast.literal_eval(str_header)

    # get response
    response = requests.request("GET", url, data=payload, headers=headers, params=querystring)

    if not response.raise_for_status():
        return response.json()
    else:
        response.raise_for_status()


def get_request_logistic(url, email, password, query_string=""):
    json_string = "'email': \"{0}\", 'password': \"{1}\"".format(email, password)
    json = ast.literal_eval("{" + json_string + "}")
    # print(json)

    url_authorization = config.DEFAULT_ENV['hq-backend'] + '/api/v2/portal/logistics/authentication'.strip()

    access_token = requests.post(url_authorization, None, json=json)
    # print(access_token)
    access_token = access_token.json()
    access_token = access_token['access_token']

    str_header = "'content-type': \"application/json\",'authorization':\
                    \"bearer {0}\",'cache-control': \"no-cache\"".format(access_token)
    str_header = "{" + str_header + "}"

    # input query to param
    querystring = ast.literal_eval(query_string)
    payload = "{\n    \"email\": \"hoangviet@msv-tech.vn\",\n    \"password\": \"12345678\"\n}"
    headers = ast.literal_eval(str_header)

    # get response
    response = requests.request("GET", url, data=payload, headers=headers, params=querystring)

    if not response.raise_for_status():
        try:
            return response.json()
        except ValueError:
            return response.text
    else:
        response.raise_for_status()


def get_request_cdc(url, email, password, query_string=""):
    json_string = "'email': \"{0}\", 'password': \"{1}\"".format(email, password)
    json = ast.literal_eval("{" + json_string + "}")

    url_authorization = config.DEFAULT_ENV['hq-backend'] + '/api/v2/portal/cdc/authentication'.strip()

    access_token = requests.post(url_authorization, None, json=json)
    access_token = access_token.json()
    access_token = access_token['access_token']

    str_header = "'content-type': \"application/json\",'authorization':\
                    \"bearer {0}\",'cache-control': \"no-cache\"".format(access_token)
    str_header = "{" + str_header + ", 'Accept':'application/json'}"

    # input query to param
    querystring = ast.literal_eval("{" + query_string + "}")
    payload = "{\n    \"email\": \"cdc101@ssv.com.vn\",\n    \"password\": \"cdc101@ssv2017\"\n}"
    headers = ast.literal_eval(str_header)

    # get response
    response = requests.request("GET", url, data=payload, headers=headers, params=querystring)

    if not response.raise_for_status():
        return response.json()
    else:
        response.raise_for_status()
