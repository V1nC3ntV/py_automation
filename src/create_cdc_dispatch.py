import csv
import get_info_purchase_order
import datetime
import time
import pandas as pd
import unidecode
import json

data_string_template = "{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13}"
TODAY                = int(time.mktime(datetime.datetime.today().timetuple()) * 1000)
TODAY_DATE           = datetime.datetime.fromtimestamp(TODAY / 1000).strftime('%Y-%m-%d')
TOMORROW             = int(time.mktime((datetime.datetime.today() + datetime.timedelta(days=1)).timetuple()) * 1000)
TOMORROW_DATE        = datetime.datetime.fromtimestamp(TOMORROW / 1000).strftime('%Y-%m-%d')


def create_cdc_dispatch_data(
        delivery_date,
        supplier_id,
        cdc_code,
        supplier_product_id,
        store_dispatch_quantity,
        dispatch_time,
        submit_time):
    print(supplier_id)
    purchase_order_data = get_info_purchase_order.get_purchase_order_detail(
        delivery_date, supplier_id)

    data = pd.DataFrame()
    for i in purchase_order_data:
        df = pd.read_json(json.dumps(i))
        df = df.drop_duplicates(['id'], keep='last')
        df = df.set_index(df.id)
        df.product_name = df.product_name.apply(
            lambda x: unidecode.unidecode(x))
        df.vendor_name = df.vendor_name.apply(lambda x: unidecode.unidecode(x))
        data = data.append(df)

    groupby = data.groupby(['cdc_code', 'product_code']).head()
    groupby = data.sort_values(['cdc_code', 'product_code'], ascending=[1, 0])

    purchas_order_cdc = groupby[(groupby.cdc_code == cdc_code)]
    purchase_order_cdc_dict = purchas_order_cdc.to_dict()

    # Create list input from purchase order data
    product_code            = purchase_order_cdc_dict['product_code']
    product_name            = purchase_order_cdc_dict['product_name']
    supplier_code           = purchase_order_cdc_dict['supplier_code']
    supplier_name           = purchase_order_cdc_dict['vendor_name']
    purchase_order_id       = purchase_order_cdc_dict['purchase_order_id']
    delivery_note_id        = purchase_order_cdc_dict['delivery_note_id']
    store_code              = purchase_order_cdc_dict['store_code']
    store_name              = purchase_order_cdc_dict['store_name']
    inventory_type          = purchase_order_cdc_dict['inventory_type']
    store_dispatch_quantity = purchase_order_cdc_dict['unit_quantity']

    product_code            = [product_code[x] for x in product_code]
    product_name            = [product_name[x] for x in product_name]
    supplier_code           = [supplier_code[x] for x in supplier_code]
    supplier_name           = [supplier_name[x] for x in supplier_name]
    purchase_order_id       = [purchase_order_id[x] for x in purchase_order_id]
    delivery_note_id        = [delivery_note_id[x] for x in delivery_note_id]
    store_code              = [store_code[x] for x in store_code]
    store_name              = [store_name[x] for x in store_name]
    inventory_type          = [inventory_type[x] for x in inventory_type]
    store_dispatch_quantity = [store_dispatch_quantity[x] for x in store_dispatch_quantity]

    # chagne inventory type into logic number
    inventory_type = [0 if x == "STOCK" else 1 for x in inventory_type]

    # Create other input from param
    cdc_code_list = list()
    supplier_product_id_list = list()
    store_dispatch_quantity_list = list(store_dispatch_quantity)
    dispatch_time_list = list()
    submit_time_list = list()

    for i in product_code:
        cdc_code_list.append(str(cdc_code))
        supplier_product_id_list.append(supplier_product_id)
        store_dispatch_quantity_list.append(store_dispatch_quantity)
        dispatch_time_list.append(dispatch_time)
        submit_time_list.append(submit_time)

    join = zip(*[product_code, product_name, supplier_code, supplier_name,
                 supplier_product_id_list, cdc_code_list, delivery_note_id,
                 store_code, purchase_order_id, store_name,
                 store_dispatch_quantity_list, dispatch_time_list,
                 submit_time_list, inventory_type])
    join = [list(row) for row in join]
    # print(join)
    return join


def create_cdc_dispatch(
        file_name=None,
        list_dispatch=None):
    """Create cdc dispatch csv"""
    with open('temp_cdc_dispatch.csv', 'w') as csvfile:
        spamwriter = csv.writer(
            csvfile, delimiter=',',
            quotechar='"',
            quoting=csv.QUOTE_NONNUMERIC)

        list_header = "product_id,product_short_name_vn,supplier_id,\
                        supplier_name,supplier_product_id,cdc_id,cdn_id,\
                        store_id,po_id,store_name,store_dispatch_quantity,\
                        dispatch_time,submit_time,inventory_type"
        list_header = map(str.strip, list_header.split(sep=','))
        spamwriter.writerow(list_header)

        if list_dispatch is not None:
            # list_dispatch = [list(row) for row in list_dispatch]
            for i in list_dispatch:
                row = map(str.strip, str(','.join(map(str, i))).split(sep=','))
                spamwriter.writerow(row)


list_supplier = get_info_purchase_order.get_supplier_list('2018-04-05')
list_full_dispatch = list()
for row in list_supplier:
    list_dispatch = create_cdc_dispatch_data(
        "2018-04-05", cdc_code=102,
        supplier_id=row['id'],
        supplier_product_id='0',
        store_dispatch_quantity='10',
        dispatch_time=TODAY,
        submit_time=TODAY)
    list_full_dispatch = list_full_dispatch + list_dispatch

# print(list_full_dispatch)
create_cdc_dispatch(list_dispatch=list_full_dispatch)
