import pandas as pd
import datetime
import time
import get_info_purchase_order
from io import StringIO
from unidecode import unidecode

TODAY         = int(time.mktime(datetime.datetime.today().timetuple()) * 1000)
TOMORROW      = int(time.mktime((datetime.datetime.today() + datetime.timedelta(days=1)).timetuple()) * 1000)
TOMORROW_DATE = datetime.datetime.fromtimestamp(TOMORROW / 1000).strftime('%Y-%m-%d')
TODAY_DATE    = datetime.datetime.fromtimestamp(TODAY / 1000).strftime('%Y-%m-%d')
EXPIRY_DATE   = int(time.mktime((datetime.datetime.today() + datetime.timedelta(days=180)).timetuple()) * 1000)

receiving_columns = [
    'product_id',
    'product_short_name_vn',
    'supplier_id',
    'supplier_name',
    'supplier_product_id',
    'expiry_date',
    'quantity',
    'cdc_id',
    'start_receiving_time',
    'end_receiving_time',
    'supplier_delivery_id',
    'submit_timestamp',
    'inventory_type']


def create_receiving_pass(delivery_date):
    purchase_order_detail_pd = pd.read_csv(
        StringIO(get_info_purchase_order.get_purchase_order_detail_export(delivery_date)))
    purchase_order_detail_pd = purchase_order_detail_pd[purchase_order_detail_pd['Inventory type'] == 'PASS']
    purchase_order_detail_pd['Total Order'] = purchase_order_detail_pd.groupby(
        by=[
            'Product code',
            'Supplier code',
            'CDC code']
    )['Store order base unit qty'].transform('sum')
    print(purchase_order_detail_pd)

    receiving_pd                          = pd.DataFrame(columns=receiving_columns)
    receiving_pd['product_id']            = purchase_order_detail_pd['Product code']
    receiving_pd['product_short_name_vn'] = purchase_order_detail_pd['Product name'].apply(lambda x: unidecode(x))
    receiving_pd['supplier_id']           = purchase_order_detail_pd['Supplier code']
    receiving_pd['supplier_name']         = purchase_order_detail_pd['Supplier name'].apply(lambda x: unidecode(x))
    receiving_pd['supplier_product_id']   = purchase_order_detail_pd['Supplier product ID']
    receiving_pd['expiry_date']           = EXPIRY_DATE
    receiving_pd['quantity']              = purchase_order_detail_pd['Total Order']
    receiving_pd['cdc_id']                = purchase_order_detail_pd['CDC code'].apply(lambda x: int(x))
    receiving_pd['start_receiving_time']  = TODAY
    receiving_pd['end_receiving_time']    = TODAY
    receiving_pd['supplier_delivery_id']  = purchase_order_detail_pd['Supplier delivery id']
    receiving_pd['submit_timestamp']      = TODAY
    receiving_pd['inventory_type']        = 1
    receiving_pd                          = receiving_pd.drop_duplicates()

    receiving_pd_cdc101 = receiving_pd[(receiving_pd['cdc_id'] == 101)].set_index('product_id')
    receiving_pd_cdc102 = receiving_pd[(receiving_pd['cdc_id'] == 102)].set_index('product_id')
    receiving_pd_cdc101.to_csv('temp_pass_receiving_cdc101.csv')
    receiving_pd_cdc102.to_csv('temp_pass_receiving_cdc102.csv')


create_receiving_pass('2018-04-18')
