import api_request
import config

url_backend = config.DEFAULT_ENV['hq-backend']
url_product_list = url_backend + "/api/v2/hq/products/"
url_supplier_list = url_backend + "/api/v2/hq/suppliers/"
url_product_supplier_cdc = url_backend + "/api/v2/hq/product_supplier_cdc/"
url_product_uom = url_backend + "/api/v2/hq/product_uoms/"

email = "hoangviet@msv-tech.vn"
password = "12345678"


def get_product_info(product_query, info):
    product_query = "'q': '{0}'".format(product_query)
    product_info = api_request.get_request(
        url_product_list,
        email=email,
        password=password,
        query_string=product_query)

    product_info_list = product_info['content']
    if not product_info_list:
        print("List is empty")
        return None
    else:
        product_info_dict = product_info_list[0]
        return product_info_dict[info]


def get_supplier_info(supplier_query, info):
    supplier_query = "'q': '{0}'".format(supplier_query)
    supplier_info = api_request.get_request(
        url_supplier_list,
        email=email,
        password=password,
        query_string=supplier_query)

    supplier_info_list = supplier_info['content']
    if not supplier_info_list:
        print("List is empty")
        return None
    else:
        supplier_info_dict = supplier_info_list[0]
        return supplier_info_dict[info]


def get_supplier_mapping_info(product_code, supplier_info):
    product_id = get_product_info(
        product_query=product_code,
        info='id')

    if product_id is None:
        return None
    else:
        url_string = url_product_supplier_cdc + str(product_id)

    response_info = api_request.get_request(url_string, email, password)
    supplier_mapping_info = response_info['supplier_mapping_info']
    supplier_mapping_info = supplier_mapping_info[0]
    return supplier_mapping_info[supplier_info]


def get_refill_uom_info(product_code, refill_info):
    product_id = get_product_info(
        product_query=product_code,
        info='id')

    if product_id is None:
        return None
    else:
        url_string = url_product_supplier_cdc + str(product_id)

    response_info = api_request.get_request(url_string, email, password)
    return response_info[refill_info]


def get_product_uom(product_code):
    product_id = get_product_info(
        product_query=product_code,
        info='id')

    if product_id is None:
        return None
    else:
        url_string = url_product_uom + str(product_id)

    response_info = api_request.get_request(url_string, email, password)
    return response_info['product_uoms']
