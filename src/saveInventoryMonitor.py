import requests
import pandas as pd
import json
import io

URL = "https://backend.sevensystem.vn/api/v2/portal/supplier/inventory_monitor/v2"
URL_EXPORT = "https://backend.sevensystem.vn/api/v2/portal/supplier/inventory_product_history/{}/export?cdc_id={}&supplier_id={}&file_type=csv"

QUERYSTRING = {"page": "1", "page_size": "2000"}
QUERYSTRING_PRODUCT = '''{{"page":"1","page_size":"2000","cdc_id":"{}","supplier_id":"{}"}}'''
QUERYSTRING_PRODUCT_INFO = '''{{"cdc_id":"{}", "supplier_id":"{}"}}'''

HEADERS = {
    'Authorization': "Bearer ddd27bd0-77d3-4ec8-94d6-465eb29509c3",
    'Accept-Encoding': "gzip, deflate, br",
    'Connection': "keep-alive",
    'Cache-Control': "no-cache"
}

RESPONSE = requests.request("GET", URL, headers=HEADERS, params=QUERYSTRING)
# print(json.loads(RESPONSE.text))

_PRODUCT = pd.DataFrame(json.loads(RESPONSE.text)['content'])
EXCEL_PD = pd.DataFrame()

for idx, x in _PRODUCT.iterrows():
    _url_export = URL_EXPORT.format(x['product_code'], x['cdc_id'], x['supplier_id'])
    _response_product = requests.request("GET", _url_export, headers=HEADERS)
    EXCEL_PD = EXCEL_PD.append(pd.read_csv(io.StringIO(_response_product.content.decode('utf-8'))))

EXCEL_FILE = pd.ExcelWriter('inventoryMonitor.xlsx')
EXCEL_PD.to_excel(EXCEL_FILE, index=False)
EXCEL_FILE.save()
