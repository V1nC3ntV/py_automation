import urllib2
import json
import requests
import config

str_hq_backend = config.DEFAULT_ENV['hq-backend']
str_url = str_hq_backend + "/api/system/authentication"
str_url_logistic = str_hq_backend + "/api/v2/portal/logistics/authentication"
str_hq_authorization = str_hq_backend + "/api/v2/hq/authentication"
str_hq_categories = str_hq_backend + "/api/v2/hq/categories?&q=&type=1&\{\}"
str_cdc_inventory_consolidate = str_hq_backend + "/api/v2/test/all_daily_inventory_consolidation"

login = json.dumps({"email": "hoangviet@msv-tech.vn", "password": "12345678"})

r = requests.post(
    str_hq_authorization, json={"email": "hoangviet@msv-tech.vn", "password": "12345678"})
print(r.json())

j = r.json()


def get_auth_token(url_string):
    '''
    get an auth token
    '''
    req = urllib2.Request(url_string, login, {'Content-Type': 'application/json'})
    response = urllib2.urlopen(req)
    html = response.read()
    json_obj = json.loads(html)
    token_string = json_obj["access_token"].encode("ascii", "ignore")
    return token_string


def get_response_json_object(url, auth_token):
    '''
        returns json object with info
    '''
    auth_token = get_auth_token(url)
    req = urllib2.Request(
        url, None, {"Content-Type": "application/json", "Authorization": "Bearer %s" % auth_token})
    response = urllib2.urlopen(req)
    html = response.read()
    json_obj = json.loads(html)
    return json_obj
