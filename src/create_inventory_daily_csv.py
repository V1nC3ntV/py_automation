import csv

import time

import get_info_master

data_string_template = "{0},{1},{2},{3},,{4},{5},0,{6},{7}"


def input_info_template(
        product_code,
        cdc_code='001',
        quantity=0,
        exp_date=int(round(time.time() * 1000))):
    """create template for csv"""
    product_name = get_info_master.get_product_info(product_code, 'short_name')
    supplier_name = get_info_master.get_supplier_mapping_info(
        product_code, 'supplier_name')
    supplier_code = get_info_master.get_supplier_info(
        supplier_name, 'supplier_code')

    data_string = data_string_template.format(
        product_code, product_name,
        supplier_code, supplier_name,
        cdc_code,
        exp_date,
        quantity,
        int(round(time.time() * 1000)))

    return data_string


def create_inventory_daily_data(data_json):
    """Create inventory daily data"""
    data = []
    product_code = 0000000
    cdc_code = '001'
    quantity = 0
    exp_date = int(round(time.time() * 1000))

    for i in data_json:
        if 'product_code' in i:
            product_code = i['product_code']
        if 'quantity' in i:
            quantity = i['quantity']
        if 'cdc_code' in i:
            cdc_code = i['cdc_code']
        if 'exp_date' in i:
            exp_date = i['exp_date']

        data_row = input_info_template(
            product_code=product_code,
            cdc_code=cdc_code,
            quantity=quantity,
            exp_date=exp_date)

        data.append(data_row)

    return data


def create_inventory_daily(
        file_name=None,
        list_product_code=None):
    """Create inventory daily"""
    with open('temp_daily.csv', 'w') as csvfile:
        spamwriter = csv.writer(
            csvfile, delimiter=',',
            quotechar='"',
            quoting=csv.QUOTE_NONNUMERIC)

        list_header = "product_id,product_short_name_vn,supplier_id,\
                        supplier_name,supplier_product_id,cdc_id,expiry_date,\
                        status,quantity,submit_time"
        list_header = map(str.strip, list_header.split(sep=','))
        spamwriter.writerow(list_header)

        if list_product_code is not None:
            data = create_inventory_daily_data(list_product_code)
            for i in data:
                row = map(str.strip, str(i).split(sep=','))
                spamwriter.writerow(row)


if __name__ == "__main__":
    create_inventory_daily(list_product_code=[
        {
            "product_code": 13001401,
            "cdc_code": "001",
            "quantity": 1000,
            "exp_date": 1507568400000
        },
        {
            "product_code": 40000001,
            "cdc_code": "001",
            "quantity": 1000,
            "exp_date": 1507568400000
        },
        {
            "product_code": 40000002,
            "cdc_code": "001",
            "quantity": 1000,
            "exp_date": 1507568400000
        },
        {
            "product_code": 40000003,
            "cdc_code": "001",
            "quantity": 1000,
            "exp_date": 1507568400000
        },
        {
            "product_code": 40000004,
            "cdc_code": "001",
            "quantity": 1000,
            "exp_date": 1507568400000
        },
        {
            "product_code": 40000005,
            "cdc_code": "001",
            "quantity": 1000,
            "exp_date": 1507568400000
        },
        {
            "product_code": 40000006,
            "cdc_code": "001",
            "quantity": 1000,
            "exp_date": 1507568400000
        },
        {
            "product_code": 40000008,
            "cdc_code": "001",
            "quantity": 1000,
            "exp_date": 1507568400000
        },
        {
            "product_code": 40000009,
            "cdc_code": "001",
            "quantity": 1000,
            "exp_date": 1507568400000
        },
        {
            "product_code": 40000011,
            "cdc_code": "001",
            "quantity": 1000,
            "exp_date": 1507568400000
        },
        {
            "product_code": 40000013,
            "cdc_code": "001",
            "quantity": 1000,
            "exp_date": 1507568400000
        },
        {
            "product_code": 40000014,
            "cdc_code": "001",
            "quantity": 1000,
            "exp_date": 1507568400000
        },
        {
            "product_code": 40000015,
            "cdc_code": "001",
            "quantity": 1000,
            "exp_date": 1507568400000
        },
        {
            "product_code": 40000016,
            "cdc_code": "001",
            "quantity": 1000,
            "exp_date": 1507568400000
        },
        {
            "product_code": 40000017,
            "cdc_code": "001",
            "quantity": 1000,
            "exp_date": 1507568400000
        },
        {
            "product_code": 25000005,
            "cdc_code": "001",
            "quantity": 1000,
            "exp_date": 1507568400000
        },
        {
            "product_code": 25000006,
            "cdc_code": "001",
            "quantity": 1000,
            "exp_date": 1507568400000
        },
        {
            "product_code": 25000007,
            "cdc_code": "001",
            "quantity": 1000,
            "exp_date": 1507568400000
        },
        {
            "product_code": 25000008,
            "cdc_code": "001",
            "quantity": 1000,
            "exp_date": 1507568400000
        },
        {
            "product_code": 10001242,
            "cdc_code": "001",
            "quantity": 1000,
            "exp_date": 1507568400000
        },
        {
            "product_code": 29000003,
            "cdc_code": "001",
            "quantity": 1000,
            "exp_date": 1507568400000
        },
        {
            "product_code": 29000004,
            "cdc_code": "001",
            "quantity": 1000,
            "exp_date": 1507568400000
        },
        {
            "product_code": 35001056,
            "cdc_code": "001",
            "quantity": 1000,
            "exp_date": 1507568400000
        },
        {
            "product_code": 35001057,
            "cdc_code": "001",
            "quantity": 1000,
            "exp_date": 1507568400000
        },
        {
            "product_code": 35001058,
            "cdc_code": "001",
            "quantity": 1000,
            "exp_date": 1507568400000
        },
        {
            "product_code": 20001061,
            "cdc_code": "001",
            "quantity": 1000,
            "exp_date": 1507568400000
        },
        {
            "product_code": 30001065,
            "cdc_code": "001",
            "quantity": 1000,
            "exp_date": 1507568400000
        },
        {
            "product_code": 45001066,
            "cdc_code": "001",
            "quantity": 1000,
            "exp_date": 1507568400000
        },
        {
            "product_code": 45001067,
            "cdc_code": "001",
            "quantity": 1000,
            "exp_date": 1507568400000
        },
        {
            "product_code": 45001068,
            "cdc_code": "001",
            "quantity": 1000,
            "exp_date": 1507568400000
        },
        {
            "product_code": 10001389,
            "cdc_code": "001",
            "quantity": 1000,
            "exp_date": 1507568400000
        },
        {
            "product_code": 30001069,
            "cdc_code": "001",
            "quantity": 1000,
            "exp_date": 1507568400000
        },
        {
            "product_code": 10001391,
            "cdc_code": "001",
            "quantity": 1000,
            "exp_date": 1507568400000
        },
        {
            "product_code": 11001267,
            "cdc_code": "001",
            "quantity": 1000,
            "exp_date": 1507568400000
        }
    ])
