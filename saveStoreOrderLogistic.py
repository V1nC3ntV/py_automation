import requests
from datetime import timedelta, date, datetime
import time
import ast
import io
import pandas as pd

url = "http://dev-hq-backend.seven-system.com/api/v2/portal/logistics/store_order/purchase_order_suppliers_export"

headers = {
    'accept-encoding': "gzip, deflate, br",
    'authorization': "Bearer ad0ff9eb-566c-4f00-b67e-1964571e6bcd",
    'Content-Type': "application/json",
    'Accept': "*/*"}


def daterange(start_date, end_date):
    for n in range(int((end_date - start_date).days)):
        yield start_date + timedelta(n)


# Start run save files
start_date = date(2018, 1, 1)
end_date = date(2018, 2, 2)
delta = timedelta(days=1)
excel_pd = pd.DataFrame()
while start_date <= end_date:
    # for single_date in daterange(start_date, end_date):
    delivery_date = int(time.mktime(start_date.timetuple())) * 1000
    delivery_date = datetime.fromtimestamp(delivery_date / 1000).strftime('%Y-%m-%d')

    querystring = '''{{"delivery_date":"{}", "file_type": "csv"}}'''.format(
        delivery_date)
    querystring = ast.literal_eval(querystring)
    print(querystring)

    response = None
    response = requests.get(url, headers=headers, params=querystring)

    # print(response.content)

    excel_pd = excel_pd.append(pd.read_csv(io.StringIO(response.content.decode('utf-8'))))
    start_date += delta


writer = pd.ExcelWriter('./temp_logistic.xlsx')
# excel_pd['Logistics fee'] = excel_pd['Logistics fee'].apply(pd.to_numeric)
excel_pd['Logistics fee'] = excel_pd['Logistics fee'].apply(lambda x: format(x, '.2f'))
# excel_pd['Logistics fee'] = excel_pd['Logistics fee'].round(2)
# excel_pd['Logistics fee'] = excel_pd['Logistics fee'].astype(str)
excel_pd.to_excel(writer, index=False)
writer.save()
