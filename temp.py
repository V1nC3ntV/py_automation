import pandas as pd


temp_db = pd.DataFrame(pd.read_excel("temp.xlsx"))
duplicated = temp_db[temp_db.duplicated(subset=['Product ID', 'Store Name', 'Delivery date'], keep=False)]
excel = pd.ExcelWriter('_temp.xlsx')
duplicated.to_excel(excel)
excel.save()
print(duplicated)
